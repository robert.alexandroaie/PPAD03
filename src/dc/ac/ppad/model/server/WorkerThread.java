package dc.ac.ppad.model.server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @author Robert
 *
 */
public class WorkerThread implements Runnable {

	private final Socket clientSocket;

	/*
	 * 
	 */
	public WorkerThread(final Socket clientSocket) {
		
		this.clientSocket = clientSocket;
	}

	/**
	 * The run method is invoked by the ExecutorService (thread pool).
	 */
	@Override
	public void run() {

		BufferedReader socketInput = null;
		DataOutputStream socketOutput = null;
		
		try {
			
			socketInput = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
			socketOutput = new DataOutputStream(this.clientSocket.getOutputStream());
			
			while (true) {

				String origLine = socketInput.readLine();
				
				if (origLine == null) {
				
					break;
				}
				String upperLine = origLine.toUpperCase() + "\n";

				System.out.println("WORKER("+clientSocket.getLocalPort()+")"+upperLine);
				
				socketOutput.write(upperLine.getBytes());
				socketOutput.flush();
			}
		} catch (IOException ioe) {

			ioe.printStackTrace();
		} finally {
		
			closeStreams(socketInput, socketOutput);
		}
	}

	/**
	 * @param userInput
	 * @param userOutput
	 */
	private void closeStreams(BufferedReader userInput, DataOutputStream userOutput) {
		
		try {
			
			if (userInput != null) {
			
				userInput.close();
			}
			
			if (userOutput != null) {
				
				userOutput.close();
			}
			
			this.clientSocket.close();
			
			System.err.println("CLIENT finished: " + this.clientSocket.getRemoteSocketAddress());
		} catch (IOException ioe2) {
			// Ignored
		}
	}
}
