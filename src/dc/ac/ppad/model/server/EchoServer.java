package dc.ac.ppad.model.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EchoServer extends Thread {

	private ServerSocket listenSocket;
	private final ExecutorService workers = Executors.newFixedThreadPool(5);
	private boolean keepRunning = true;

	/**
	 * 
	 * @param port
	 */
	public EchoServer(final int port) {

		addShutdownHook();

		try {

			listenSocket = new ServerSocket(port);
		} catch (IOException e) {

			System.err.println("An exception occurred while creating the listen socket: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * 
	 */
	private void addShutdownHook() {
		
		Runtime.getRuntime().addShutdownHook(new Thread() {

			@Override
			public void run() {

				shutdown();
			}
		});
	}


	/**
	 * 
	 */
	private void shutdown() {
		
		System.out.println("Shutting down the server.");

		keepRunning = false;
		workers.shutdownNow();

		try {

			join();
		} catch (InterruptedException e) {

			// TODO handle exception
		}
	}
	
	/**
	 * 
	 */
	@Override
	public void run() {

		setTimeout();

		System.out.println("Accepting incoming connections on port " + this.listenSocket.getLocalPort());

		while (keepRunning) {

			try {

				final Socket clientSocket = this.listenSocket.accept();
				System.out.println("Accepted connection from " + clientSocket.getRemoteSocketAddress());

				WorkerThread handler = new WorkerThread(clientSocket);
				workers.execute(handler);

			} catch (SocketTimeoutException te) {

				// TODO handle exception
			} catch (IOException ioe) {

				System.err.println("Exception occurred while handling client request: " + ioe.getMessage());
				Thread.yield();
			}
		}

		closeStream();
	}

	/**
	 * 
	 */
	private void setTimeout() {
		try {

			listenSocket.setSoTimeout(1000);
		} catch (SocketException e1) {

			System.err.println("Unable to set acceptor timeout value.  The server may not shutdown gracefully.");
		}
	}

	/**
	 * 
	 */
	private void closeStream() {
		
		try {

			listenSocket.close();
		} catch (IOException ioe) {

			// TODO handle exception
		}
		System.out.println("Stopped accepting incoming connections.");
	}

}