package dc.ac.ppad.model.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

	public void connectAndAsk() throws UnknownHostException, IOException {

		String sentence;
		BufferedReader inFromUser = getBufferedInputFromKeyboard();
		Socket clientSocket = getSocketToServer();

		DataOutputStream outToServer = null;
		BufferedReader inFromServer = null;

		try {
			outToServer = new DataOutputStream(clientSocket.getOutputStream());
			inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			sentence = inFromUser.readLine();

			outToServer.writeBytes(sentence + '\n');
			outToServer.flush();
			
			String modifiedSentence = inFromServer.readLine();
			System.out.println("IN CLIENT: " + modifiedSentence);

		} catch (Exception e) {

			e.printStackTrace();
		} finally {

			try {
				
				if (outToServer != null) {

					outToServer.close();
				}

				if (inFromServer != null) {

					inFromServer.close();
				}
				clientSocket.close();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/**
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	private Socket getSocketToServer() throws UnknownHostException, IOException {
		return new Socket("localhost", 666);
	}

	/**
	 * @return
	 */
	private BufferedReader getBufferedInputFromKeyboard() {
		return new BufferedReader(new InputStreamReader(System.in));
	}
}
