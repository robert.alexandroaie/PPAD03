package dc.ac.ppad.main;

import java.io.IOException;
import java.net.UnknownHostException;

import dc.ac.ppad.model.client.Client;

public class ClientMain {

	public static void main(String[] args) {
		
		startClients();
	
	}
	
	/**
	 * 
	 */
	private static void startClients() {
		
		try {
			
			for(int i=0;i<10;i++) {
				
				new Client().connectAndAsk();
			}
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
		} catch(IOException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
