package dc.ac.ppad.main;

import dc.ac.ppad.model.server.EchoServer;

public class Main {

	public static void main(String[] args) {

		int port = getPort(args);

		final EchoServer server = new EchoServer(port);
		server.start();

		handleShutdown(server);

	}

	/**
	 * @param args
	 * @return
	 */
	private static int getPort(String[] args) {

		validateArgs(args);

		int port = getPortFromArgs(args);

		validatePort(port);

		return port;
	}

	/**
	 * @param port
	 */
	private static void validatePort(int port) {

		if (port <= 0 || port > 65536) {

			System.err.println("Port value must be in (0, 65535].");
			System.exit(1);
		}
	}

	/**
	 * @param args
	 * @return
	 */
	private static int getPortFromArgs(String[] args) {

		int port = -1;

		try {

			port = Integer.parseInt(args[0]);
		} catch (NumberFormatException nfe) {

			System.err.println("Invalid listen port value: \"" + args[1] + "\".");
			printUsageMessage();
			System.exit(1);
		}
		return port;
	}

	/**
	 * @param args
	 */
	private static void validateArgs(String[] args) {

		if (args.length < 1) {

			printUsageMessage();
			System.exit(1);
		}
	}

	/**
	 * @param server
	 */
	private static void handleShutdown(final EchoServer server) {

		try {

			server.join();
			System.out.println("Completed shutdown.");
		} catch (InterruptedException e) {

			System.err.println("Interrupted before accept thread completed.");
			System.exit(1);
		}
	}

	/**
	 * 
	 */
	private static void printUsageMessage() {

		System.err.println("Echo server requires 1 argument: <Listen Port>");
	}
}
